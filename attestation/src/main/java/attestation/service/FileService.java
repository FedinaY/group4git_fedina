package attestation.service;

import java.io.*;
import java.util.ArrayList;

public class FileService {

    public String fileReader(FileReader textFile) {
        String readData = "";

        try (BufferedReader bufferedReader = new BufferedReader((textFile))) {

            String line = null;
            String repeat = "";

            while ((line = bufferedReader.readLine()) != null) {
                String[] info = line.split(" ");
                for (int i = 0; i < info.length; i++) {
                    int numberOfWords = 1;
                    for (int j = i + 1; j < info.length; j++) {
                        if (info[i].equals(info[j])) {
                            numberOfWords = numberOfWords + 1;
                        }
                    }
                    if (!(info[i].equals(repeat))) {
                        if (readData != "") {
                            readData = readData + "\n" + info[i] + " - " + numberOfWords;
                        } else {
                            readData = info[i] + " - " + numberOfWords;
                        }
                    }

                    for (int j = i + 1; j < info.length; j++) {
                        if (info[i].equals(info[j])) {
                            repeat = info[i];
                        }
                    }

                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return readData;
    }

    public void writeFile(String information) {

        try (Writer writer = new FileWriter("attestation/src/main/java/attestation/finalFile.txt")) {

            writer.write(String.valueOf(information));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
