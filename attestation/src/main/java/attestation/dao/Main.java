package attestation.dao;

import attestation.service.FileService;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        FileReader textFile = new FileReader("attestation/src/main/java/attestation/sourceText.txt");
        FileService fileService = new FileService();
        String readFile = fileService.fileReader(textFile);
        System.out.println(readFile);
        fileService.writeFile(readFile);
    }
}
