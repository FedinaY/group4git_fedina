package HomeWork3;

import java.util.Arrays;

public class HomeWork3 {
    public static void main(String[] args) {

        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        swapArray(array);

        int[] array2 = {25, 7, 5, 78, 1, 6};
        int index = returnTheIndex(array2, 5);
        System.out.println(index);
    }

    //Процедура, которая переместит все значимые элементы влево, заполнив нулевые
    public static void swapArray(int[] array) {
        int[] swapArray = new int[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                swapArray[j] = array[i];
                j++;
            }
        }
        System.out.println(Arrays.toString(swapArray));
    }

    //Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть индекс
    // этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
    public static int returnTheIndex(int[] array, int number) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }
        }
        return -1;

    }
}
