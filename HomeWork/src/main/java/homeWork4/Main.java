package homeWork4;

public class Main {
    public static void main(String[] args) {

        Figure[] figures = new Figure[4];
        figures[0] = new Square();
        figures[1] = new Circle();
        figures[2] = new Ellipse();
        figures[3] = new Rectangle();
        for (Figure figure : figures) {
            if (figure instanceof Moveable) {
                ((Moveable) figure).move(1, 2);
            }
            System.out.println(figure.getPerimeter());
        }
    }
}
