package homeWork4;

public class Circle extends Ellipse implements Moveable{

    @Override
    public void move(int newX, int newY) {

        x = newX;
        y = newY;
    }

    @Override
    int getPerimeter() {
        return (int) (2 *  Math.PI * x);
    }
}
