package homeWork4;

public abstract class Figure {
    public int y;
    public int x;

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    abstract int getPerimeter();


}
