package homeWork5;


public class StaticMethods {
    public static int resultSum(int... number) {
        int result = 0;
        for (int i = 0; i < number.length; i++) {
            result = result + number[i];
        }

        return result;
    }

    public static int resultSubtraction(int... number) {
        int[] array = bubbleSort(number);
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                result = array[i];
            } else {
                result = result - array[i];
            }

        }
        return result;
    }

    // Отсортировать массив чисел
    public static int[] bubbleSort(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        return arr;
    }

    public static int resultMultiplication(int... number) {
        int result = 1;
        for (int i = 0; i < number.length; i++) {
            result = result * number[i];
        }
        return result;
    }

    public static int resultDivision(int... number) {
        int result = 0;
        for (int i = 0; i < number.length; i++) {
            if (i == 0) {
                result = number[i];
            } else {
                if (result > number[i] && number[i] > 0) {
                    result = result / number[i];
                }
            }
        }
        return result;
    }

    public static int resultFactorial(int number) {

        if (number < 0) {
            System.out.println("Число отрицательное, факториал не будет рассчитан!");
            return 0;
        } else {
            if (number <= 1) {
                return 1;
            } else {
                return number * resultFactorial(number - 1);

            }
        }
    }
}
