package homeWork5;

public class Main {
    public static void main(String[] args) {

        System.out.println("Сложение всех чисел: " + StaticMethods.resultSum(10, 5, 25, 1));

        System.out.println("Вычитание всех чисел: " + StaticMethods.resultSubtraction(10, 5, 50));

        System.out.println("Умножение всех чисел: " + StaticMethods.resultMultiplication(2, 5, 3));

        System.out.println("Деление всех чисел: " + StaticMethods.resultDivision(50, 2, 5));

        int factorial = StaticMethods.resultFactorial(4);

        if (factorial != 0) {

            System.out.println("Факториал числа: " + factorial);
        }
    }
}
