package homeWork7.dao;

import homeWork7.model.User;

public interface UsersRepositoryFile {

    User findById(int id);
    void create(User user);
    void update(User user);
    void delete(int id);
}
