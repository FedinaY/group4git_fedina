package homeWork7.dao;

import homeWork7.dao.UsersRepositoryFileImpl;
import homeWork7.model.User;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        UsersRepositoryFileImpl usersRepositoryFileImpl = new UsersRepositoryFileImpl();

        // Создать пользователя
        User user = new User(1, "Kiril", "Petrov", 10, false);
        usersRepositoryFileImpl.create(user);

        // Найти пользователя по id
        System.out.println(usersRepositoryFileImpl.findById(1) != null
                ? usersRepositoryFileImpl.findById(1): "Пользователь не найден!");

        // Обновление информации по юзеру
        User user1 = usersRepositoryFileImpl.findById(1);
        user1.setName("Мишель");
        user1.setAge(27);
        usersRepositoryFileImpl.update(user1);

        // Удаление юзера по ID
        usersRepositoryFileImpl.delete(3);

    }

}
