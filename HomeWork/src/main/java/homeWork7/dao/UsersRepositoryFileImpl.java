package homeWork7.dao;

import homeWork7.model.User;
import homeWork7.service.FileService;
import homeWork7.service.UserService;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {

    FileService fileService = new FileService();
    UserService userService = new UserService();

    @Override
    public User findById(int id) {
        String userString = fileService.readFile(id);
        if (userString != null) {
            User user = userService.getUser(userString);
            return user;
        }
        return null;
    }

    @Override
    public void create(User user) {

        if(findById(user.getId()) != null) {
            System.out.println("Пользователь с таким id уже существует!");
            return;
        }
        List<User> users = new ArrayList<>();
        users.add(user);
        userService.writeUser(users);

    }

    @Override
    public void update(User user) {

        create(user);

    }

    @Override
    public void delete(int id) {
        fileService.deleteStringFile(id);
    }
}
