package homeWork7.service;

import homeWork7.model.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileService {

    public String readFile(int id) {

        String UserOnId = null;
        for (User user : findAll())
            if (id == user.getId()) {
                UserOnId = String.valueOf(user);
            }

        return UserOnId;
    }


    public ArrayList<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("HomeWork/src/main/java/homeWork7/UsersRepository.txt"))) {

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] info = line.split("\\|");
                int idS = Integer.parseInt(info[0]);
                String name = info[1];
                String lastname = info[2];
                int age = Integer.parseInt(info[3]);
                boolean gotAJjob = Boolean.parseBoolean(info[4]);
                users.add(new User(idS, name, lastname, age, gotAJjob));

            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return users;
    }

    public void deleteStringFile(int id) {

        UserService userService = new UserService();

        String UserOnId = null;
        List<User> users = new ArrayList<>();
        for (User user : findAll())
            if (id != user.getId()) {
                users.add(user);
            }
        File file = new File("HomeWork/src/main/java/homeWork7/UsersRepository.txt");
        file.delete();
        userService.writeUser(users);

    }
}


