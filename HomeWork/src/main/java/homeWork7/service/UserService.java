package homeWork7.service;

import homeWork7.model.User;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.List;

public class UserService {

    public User getUser(String userS) {

        String[] split = userS.split("\\|");
        User user = new User();
        user.setId(Integer.parseInt(split[0]));
        user.setName(split[1]);
        user.setLastName(split[2]);
        user.setAge(Integer.parseInt(split[3]));
        user.setGotAJjob(Boolean.parseBoolean(split[4]));

        return user;

    }

    public void writeUser(List<User> user) {

        try (Writer writer = new FileWriter("HomeWork/src/main/java/homeWork7/UsersRepository.txt")) {
            Iterator<User> iterator = user.iterator();
            while (iterator.hasNext()) {
                writer.write(iterator.next().toString() + "\n");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
