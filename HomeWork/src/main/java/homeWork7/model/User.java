package homeWork7.model;

public class User {

    private int id;
    private String name;
    private String lastName;
    private int age;
    private boolean gotAJjob;


    public User(int id, String name, String lastName, int age, boolean gotAJjob) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.gotAJjob = gotAJjob;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName(String name) {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName(String lastName) {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge(int age) {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGotAJjob() {
        return gotAJjob;
    }

    public void setGotAJjob(boolean gotAJjob) {
        this.gotAJjob = gotAJjob;
    }

    @Override
    public String toString() {
        return id + "|"
                + name + "|"
                + lastName + "|"
                + age + "|"
                + gotAJjob;
    }
}
